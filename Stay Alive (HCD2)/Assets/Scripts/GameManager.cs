﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;
    public float restartDelay = 3f;
   public void GameOver()
   {
       if (gameHasEnded == false)
       {
           gameHasEnded = true;
           Invoke("Next", restartDelay);
       }
   }
   
   void Next()
   {
       SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
   }
}
