﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewestPlayerScript : MonoBehaviour
{
    public Rigidbody2D rb;
    Vector2 movement;
    public Transform target;
    public float range = 15f, movementSpeed = 5f, rotationSpeed = 10f;
    public string enemyTag = "Enemy";
    public Transform partToRotate;
    public bool powerup = false;
    public bool isDead = false;

    private Inventory inventory;

    [Header("Shoot Mechanics")]
    public float fireRate = 0.01f;
    private float fireCountdown = 0f;
    public GameObject bulletPrefab;
    public Transform firePoint;

    [Header("Player Died CHECKER")]
    public GameObject timer;
    public RectTransform timerRect;
    private GameObject endMenu;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        endMenu = GameObject.FindGameObjectWithTag("EndMenu");
        timer = GameObject.FindGameObjectWithTag("TimerText");
        timerRect = timer.GetComponent<RectTransform>();
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        if (powerup == true) {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
            
            float shortestDistance = Mathf.Infinity;
            GameObject nearestEnemy = null;
            foreach (GameObject enemy in enemies) {
                float distanceToEnemy = Vector2.Distance(transform.position, enemy.transform.position);

                if (distanceToEnemy < shortestDistance) { //enemy found (closer)
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }

            if(nearestEnemy != null && shortestDistance <= range) { //closest target found within the range
                target = nearestEnemy.transform; 
            } else {
                target = null;
            }
        }
    }

    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (powerup == true) {
            
            Vector3 direction = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
            partToRotate.rotation = Quaternion.Euler(0f, 0f, rotation.z);

            //VECTOR2.MOVETOWARDS BETTER
            //calculate direction between enemy and firePoint and add radius offset
            //move firePoint's position = better than rotation

            if (fireCountdown <= 0f) {
                Shoot();
                fireCountdown = 1f / fireRate;
            }

            fireCountdown -= Time.deltaTime;
            //CountdownStart(); <- replaced with PowerupOn with invoke method for 10 secs
        }
    }

    void FixedUpdate()
    {
        movement.Normalize();
        rb.MovePosition(rb.position + movement * movementSpeed * Time.fixedDeltaTime);
    }

    void Shoot()
    {
        GameObject _bullet = (GameObject) Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        NewBullet bullet = _bullet.GetComponent<NewBullet>();

        if (bullet != null)
            bullet.Seek(target);
    }

    /*void CountdownStart()
    {
        StartCoroutine(PowerupOff());
    }
    
    //powerup turns off after 10 secs
    private IEnumerator PowerupOff()
    {
        yield return new WaitForSeconds(10);
        powerup = false;
    }*/

    public void PowerupOn()
    {
        //if inventory space is full (powerup in inventory) then u can call this, if false it wont do anything
        //go through every slot and stop this function when it reaches the first isFull[i] that's true
        for (int i = 0; i < inventory.slots.Length; i++) {
            if (inventory.isFull[i] == true) {
                 if (!powerup) {
                     powerup = true;
                     Destroy(inventory.slots[i].transform.GetChild(0).gameObject);
                    Invoke("ResetPowerup", 10f);
                    return;
                 }
            }
        }
    }

    void ResetPowerup()
    {
        powerup = false;
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if(other.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
            this.gameObject.SetActive(false);
            isDead = true;
            FindObjectOfType<GameManager>().GameOver();
        }
        if (other.CompareTag("border")) {
            this.gameObject.SetActive(false);
            isDead = true;
           FindObjectOfType<GameManager>().GameOver();
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

}
