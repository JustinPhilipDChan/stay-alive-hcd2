﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    private Inventory inventory;
    private NewestPlayerScript player;
    public int i;
    void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<NewestPlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        //after destroying child, this if statement allows the slots to pick up items again
        if (transform.childCount <= 0) {
            inventory.isFull[i] = false;
        }
    }
}
