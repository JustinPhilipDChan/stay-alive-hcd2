﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenToAudio : MonoBehaviour
{
    private NewestPlayerScript player;
    public MicInput mic;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<NewestPlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        float db = mic.LinearToDecibel(MicInput.MicLoudness);
        
        if (db < 1 && db > -20f) {
            player.PowerupOn();
        }

        //Debug.Log("Volume: " + MicInput.MicLoudness.ToString("##.####" + ", Decibels: " + db.ToString()));
    }
}
