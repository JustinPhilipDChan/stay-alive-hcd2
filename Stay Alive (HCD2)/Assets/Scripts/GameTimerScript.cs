﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimerScript : MonoBehaviour
{
    public Text gameTimerText;
    private string gameTime;
    float gameTimer = 0f;
    public NewestPlayerScript player;

    [Header("Time")]
    public int seconds, minutes, hours;
    
    void Start()
    {
        gameTime = PlayerPrefs.GetString("gameTimeName");
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<NewestPlayerScript>();
    }

    void Update()
    {
        if (player.isDead) {
        gameTime = string.Format("{0:0}:{1:00}:{2:00}", hours, minutes, seconds);
        gameTimerText.text = gameTime;
        PlayerPrefs.SetString("gameTimeName", gameTimerText.text);
        return;
        
        } else {
            gameTimer += Time.deltaTime;
        seconds = (int) (gameTimer % 60);
        minutes = (int) (gameTimer / 60) % 60;
        hours = (int) (gameTimer / 3600) % 60;

        gameTime = string.Format("{0:0}:{1:00}:{2:00}", hours, minutes, seconds);
        gameTimerText.text = gameTime;
        PlayerPrefs.SetString("gameTimeName", gameTimerText.text);
        }
    }

    public void timeUpdate(string gameTimeName)
    {
        gameTime = gameTimeName;
    }
}
