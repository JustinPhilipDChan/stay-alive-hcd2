﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private Inventory inventory;
    public GameObject itemPicture;
    void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    public void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Player") {
           for (int i = 0; i < inventory.slots.Length; i++) {
               if (inventory.isFull[i] == false) {
                   //item can be added
                   inventory.isFull[i] = true;
                   Instantiate(itemPicture, inventory.slots[i].transform, false);
                   Destroy(gameObject);
                   break;
               }
           }
        }
    }
}
