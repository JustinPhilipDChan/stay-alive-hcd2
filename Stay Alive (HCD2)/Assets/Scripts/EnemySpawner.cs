﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    public Vector2 center, size;
    [Header ("Spawn Restrictions")]
    public float spawnTime = 15f;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int enemiesSpawned = 0;
    public int maxEnemies;
    
    public bool stop;

    private NewestPlayerScript player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<NewestPlayerScript>();
        StartCoroutine("Spawner", spawnTime);
    }
    void Update()
    {
       spawnWait = Random.Range(spawnLeastWait, spawnMostWait);

       if (maxEnemies <= enemiesSpawned || player.isDead == true) {
           stop = true;
           Destroy(enemy);
       } else {
           stop = false;
       }
    }

    IEnumerator Spawner()
    {
        yield return new WaitForSeconds(spawnTime);

        while (!stop) {
            Vector2 pos = center + new Vector2(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2));
            Instantiate(enemy, pos, Quaternion.identity);
            enemiesSpawned++;
            yield return new WaitForSeconds(spawnWait);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.grey;
        Gizmos.DrawCube(center, size);
    }
}
