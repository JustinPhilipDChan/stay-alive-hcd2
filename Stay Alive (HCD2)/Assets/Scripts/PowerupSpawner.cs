﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : MonoBehaviour
{
    public GameObject powerupPrefab;
    public Vector2 center, size;
    public float spawnTime = 15f, spawnWait, spawnMostWait, spawnLeastWait;
    public bool stop;

    void Start()
    {
        StartCoroutine("Spawner", spawnTime);
    }
    void Update()
    {
       spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }

    IEnumerator Spawner()
    {
        yield return new WaitForSeconds(spawnTime);

        while (!stop) {
            Vector2 pos = center + new Vector2(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2));
            Instantiate(powerupPrefab, pos, Quaternion.identity);
            yield return new WaitForSeconds(spawnWait);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawCube(center, size);
    }
}
